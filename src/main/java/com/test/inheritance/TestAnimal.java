/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.inheritance;

/**
 *
 * @author patthamawan
 */
public class TestAnimal {
    public static void main(String[] args){
        Animal animal = new Animal ("Ani","White", 0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang","Black&White");
        dang.speak();
        dang.walk();
        System.out.println("---------------");
        System.out.println("dang is Animal: "+ (dang instanceof Animal));
        System.out.println("dang is Duck: "+ (dang instanceof Dog));
        System.out.println("dang is Cat: "+ (dang instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        System.out.println("---------------");
        
        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();
        System.out.println("---------------");
        System.out.println("zero is Animal: "+ (zero instanceof Animal));
        System.out.println("zero is Duck: "+ (zero instanceof Cat));
        System.out.println("zero is Cat: "+ (zero instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        System.out.println("---------------");
        
        Duck zom = new Duck("Zom", "Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        System.out.println("---------------");
        System.out.println("Zom is Animal: "+ (zom instanceof Animal));
        System.out.println("Zom is Duck: "+ (zom instanceof Duck));
        System.out.println("Zom is Cat: "+ (zom instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        System.out.println("---------------");
        
        Duck gabgab = new Duck("Gabgab", "Black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        System.out.println("---------------");
        System.out.println("Zom is Animal: "+ (zom instanceof Animal));
        System.out.println("Zom is Duck: "+ (zom instanceof Duck));
        System.out.println("Zom is Cat: "+ (zom instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        System.out.println("---------------");
        
        Dog to = new Dog("To","Brown");
        to.speak();
        to.walk();
        System.out.println("---------------");
        System.out.println("to is Animal: "+ (to instanceof Animal));
        System.out.println("to is Duck: "+ (to instanceof Dog));
        System.out.println("to is Cat: "+ (to instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        System.out.println("---------------");
        
        Dog mome = new Dog("Mome","Black&White");
        mome.speak();
        mome.walk();
        System.out.println("---------------");
        System.out.println("mome is Animal: "+ (mome instanceof Animal));
        System.out.println("mome is Duck: "+ (mome instanceof Dog));
        System.out.println("mome is Cat: "+ (mome instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        System.out.println("---------------");
        
        Dog bat = new Dog("Bat","Black&White");
        bat.speak();
        bat.walk();
        System.out.println("---------------");
        System.out.println("bat is Animal: "+ (bat instanceof Animal));
        System.out.println("bat is Duck: "+ (bat instanceof Dog));
        System.out.println("bat is Cat: "+ (bat instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        System.out.println("---------------");
       
        
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = zom;
        ani2 = zero;
        
        
        System.out.println("Animal: zom is Duck "+ (ani1 instanceof Duck));
        
        
        Animal[] animals = {dang, zero, zom, gabgab, to, mome, bat};
        for(int i=0; i<animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        }
    }
    
}
